package de.belmega.subscription.category.service;

import de.belmega.subscription.category.dao.CategoryDAOMock;
import de.belmega.subscription.category.entities.CategoryEntity;
import de.belmega.subscription.category.entities.CategoryPathEntity;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsEqual.equalTo;


/**
 * Tests for the logic to calculate the category path. (Created by test-driven development)
 */
public class CategoryPathServiceTest {

    /**
     * The service under test.
     */
    private CategoryPathService service;

    /**
     * Before each test run, create a new instance of the service under test.
     */
    @BeforeMethod
    public void setup(){
        service = new CategoryPathService();
        service.categoryDAO = new CategoryDAOMock(); // instead of injecting a real DAO, set the mock implementation
    }


    /**
     * If the category has no super category,
     * calling createPath() on it should return a path that contains exactly that category (by code).
     */
    @Test
    public void testThatThePathContainsTheCategory() throws Exception {
        //arrange
        CategoryEntity category1 = createNewCategory("engineering", "Engineering");

        //act
        CategoryPathEntity path = service.createPath(category1);

        //assert
        List<String> codes = path.getCodes();
        assertThat(codes, hasItem(category1.getCode()));
        assertThat(codes.size(), is(equalTo(1)));
    }


    /**
     * If the category has a super category which has no more super category,
     * calling createPath() on it should return a path that contains exactly two categories, including the supercategory.
     */
    @Test
    public void testThatThePathContainsTheSuperCategory() throws Exception {
        //arrange
        CategoryEntity category1 = createNewCategory("engineering", "Engineering");
        CategoryEntity category2 = createNewCategory("science", "Science");
        category1.setSuperCategoryCode(category2.getCode());

        //act
        CategoryPathEntity path = service.createPath(category1);

        //assert
        List<String> codes = path.getCodes();
        assertThat(codes, hasItem(category1.getCode()));
        assertThat(codes, hasItem(category2.getCode()));
        assertThat(codes.size(), is(equalTo(2)));
    }

    @Test
    public void testThatThePathContainsSuperCategoriesInCorrectOrder() throws Exception {
        //arrange
        CategoryEntity category0 = createNewCategory("science", "Science");
        CategoryEntity category1 = createNewCategory("engineering", "Engineering");
        CategoryEntity category2 = createNewCategory("software", "Software");

        category1.setSuperCategoryCode(category0.getCode());
        category2.setSuperCategoryCode(category1.getCode());

        //act
        CategoryPathEntity path = service.createPath(category2);

        //assert
        List<String> codes = path.getCodes();
        assertThat(codes, contains(category0.getCode(), category1.getCode(), category2.getCode()));
    }


    /**
     * In case we create a circular hierarchy of categories, we want the service to throw an exception.
     * The "expectedExceptions" property tells TestNG that the test is (only) sucessful when the exception occurs.
     */
    @Test(expectedExceptions = CircularHierarchyException.class)
    public void testThatCircularHiearchyThrowsAnException() throws Exception {
        //arrange
        CategoryEntity category1 = createNewCategory("engineering", "Engineering");
        CategoryEntity category2 = createNewCategory("science", "Science");
        category1.setSuperCategoryCode(category2.getCode());
        category2.setSuperCategoryCode(category1.getCode());

        //act
        service.createPath(category1);

        //no assert, expect exception
    }


    /**
     * Utility method to create a new CategoryEntity and put it into the mock database.
     */
    private CategoryEntity createNewCategory(String code, String title) {
        CategoryEntity category1 = new CategoryEntity(code, title);
        service.categoryDAO.createNewCategory(category1);
        return category1;
    }
}
