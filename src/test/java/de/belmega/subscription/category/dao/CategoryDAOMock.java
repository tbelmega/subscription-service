package de.belmega.subscription.category.dao;

import de.belmega.subscription.category.entities.CategoryEntity;

import javax.enterprise.inject.Alternative;
import java.util.HashMap;
import java.util.Map;

/**
 * This is a mock implementation of {@link CategoryDAO} to prevent the tests from actually calling the database.
 */
@Alternative
public class CategoryDAOMock extends CategoryDAO {

    /**
     * To set up a test, we can simply add some categories and their codes here.
     * Afterwards they are "queryable" like there was a database.
     */
    private Map<String, CategoryEntity> data = new HashMap<>();

    @Override
    public CategoryEntity findByCode(String categoryCode) {
        return data.get(categoryCode);
    }

    @Override
    public CategoryEntity createNewCategory(CategoryEntity category) {
        data.put(category.getCode(), category);
        return  category;
    }
}
