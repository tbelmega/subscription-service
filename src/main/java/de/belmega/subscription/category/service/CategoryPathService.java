package de.belmega.subscription.category.service;

import de.belmega.subscription.category.dao.CategoryDAO;
import de.belmega.subscription.category.dao.CategoryPathDAO;
import de.belmega.subscription.category.entities.CategoryEntity;
import de.belmega.subscription.category.entities.CategoryPathEntity;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class CategoryPathService {

    @Inject
    CategoryDAO categoryDAO;

    @Inject
    private CategoryPathDAO pathDAO;

    /**
     * Create a CategoryPathEntity by calculation from the given categoryEntity.
     */
    CategoryPathEntity createPath(CategoryEntity categoryEntity) {
        CategoryPathEntity categoryPathEntity = new CategoryPathEntity();
        categoryPathEntity.setRootCode(categoryEntity.getCode());

        List<String> superCategoryPath = getSuperCategoryPath(categoryEntity.getCode(), new ArrayList<>());
        categoryPathEntity.setCodes(superCategoryPath);

        return categoryPathEntity;
    }

    /**
     * Recursively create the list representing the category hierachy:
     * If the given category code is null or empty, return an empty list.
     * Else call this method for the categorie's supercategory code to get a list, and add this category.
     *
     * @deprecated use this.getSuperCategoryPath(String, List<String>) instead
     */
    @Deprecated // This method is vulnerable to circular hierarchies. (Stack Overflow Error)
    private List<String> getSuperCategoryPath(String categoryCode) {

        if (StringUtils.isEmpty(categoryCode)) {
            return new ArrayList<>();
        } else {
            CategoryEntity categoryEntity = categoryDAO.findByCode(categoryCode);

            String superCategoryCode = categoryEntity.getSuperCategoryCode();
            List<String> superCategoryPath = getSuperCategoryPath(superCategoryCode); // recursive call

            superCategoryPath.add(categoryCode);
            return superCategoryPath;
        }
    }

    /**
     * Recursively create the list of the category hierachy:
     * If the given category code is null or empty, return an empty list.
     * Else call this method for the categorie's supercategory code to get a list, and add this category.
     *
     * @param categoryCode the category code to process
     * @param circuitBreaker this list is used as an accumulator. it collects the already processed codes and breaks a circle by throwing an exception.
     */
    private List<String> getSuperCategoryPath(String categoryCode, List<String> circuitBreaker) {

        if (StringUtils.isEmpty(categoryCode)) {
            return new ArrayList<>();
        } else {
            CategoryEntity categoryEntity = categoryDAO.findByCode(categoryCode);
            String superCategoryCode = categoryEntity.getSuperCategoryCode();

            // If the superCategoryCode is already in the accumulator, break the circle by an Exception
            if (circuitBreaker.contains(superCategoryCode)) throw new CircularHierarchyException();

            circuitBreaker.add(superCategoryCode);
            List<String> superCategoryPath = getSuperCategoryPath(superCategoryCode, circuitBreaker); // recursive call

            superCategoryPath.add(categoryCode);
            return superCategoryPath;
        }
    }

    /**
     * Create new CategoryPathEntity with the path of the given category.
     * Stores it in the database.
     * @param category
     */
    public void createNewPath(CategoryEntity category) {
        CategoryPathEntity path = this.createPath(category);
        pathDAO.createNewPath(path);
    }
}
