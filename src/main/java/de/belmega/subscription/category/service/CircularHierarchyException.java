package de.belmega.subscription.category.service;

/**
 * This exception is thrown if a set of category relations forms a circular graph.
 * The exception breaks the infinite recursion to prevent a stack overflow.
 */
public class CircularHierarchyException extends RuntimeException {
}
