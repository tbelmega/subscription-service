package de.belmega.subscription.category.rest;


import de.belmega.subscription.category.dao.CategoryDAO;
import de.belmega.subscription.category.entities.CategoryEntity;
import de.belmega.subscription.category.entities.CategoryPathEntity;
import de.belmega.subscription.category.service.CategoryPathService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityExistsException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

@RequestScoped
@Path(CategoriesResource.PATH)
public class CategoriesResource {

    /**
     * The path within the server's context root, where requests are mapped to this class.
     */
    public static final String PATH = "/categories";


    @Inject
    private CategoryDAO categoryDAO;

    @Inject
    private CategoryPathService categoryPathService;

    /**
     * Takes a Category representation from the client and creates an Entity in the database.
     * @return the new representation and the URL where the new category can be fetched.
     *
     * @throws EntityExistsException if a category with the given code exists
     * @throws IllegalArgumentException if no category with the given super category code exists
     * Should not actually throw URISyntaxException or CircularHierarchyException, since there is no user input that could trigger those.
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCategory(CategoryEntity category) throws Exception {

        category = categoryDAO.createNewCategory(category);
        categoryPathService.createNewPath(category);

        URI location = determineLocationOfNewCategory(category);
        return Response.created(location).entity(category).build();
    }

    /**
     * Build the URI of the newly created category-resource, so the link can be set
     * to the responses location header.
     */
    private URI determineLocationOfNewCategory(CategoryEntity category) throws URISyntaxException {
        return new URI(PATH + "/" + category.getCode());
    }
}
