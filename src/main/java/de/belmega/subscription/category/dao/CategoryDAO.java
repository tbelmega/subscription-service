package de.belmega.subscription.category.dao;

import de.belmega.subscription.category.entities.CategoryEntity;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * The categoryDAO (database access object) contains all the code to access
 * the Category database table.
 */
@Transactional // Let JPA handle the transactions
public class CategoryDAO {


    /**
     * The persistence context is JPA's configuration of a database.
     * From that, JPA gives us an EntityManager as the tool to actually access the database.
     */
    @PersistenceContext
    private EntityManager em;

    public CategoryEntity createNewCategory(CategoryEntity category) {
        checkPreconditions(category);

        em.persist(category); // "Happy path": write the new category into the database

        return category;
    }

    /**
     * Check that no entity with that code exists before.
     * Check that the given super category does exist.
     */
    private void checkPreconditions(CategoryEntity category) {
        if (em.find(CategoryEntity.class, category.getCode()) != null) {
            throw new EntityExistsException("A category with this code already exists.");
        }

        String superCategoryCode = category.getSuperCategoryCode();
        if (StringUtils.isNotEmpty(superCategoryCode) && em.find(CategoryEntity.class, superCategoryCode) == null) {
            throw new IllegalArgumentException("No super category with the given code in the database.");
        }
    }

    public CategoryEntity findByCode(String categoryCode) {
        return em.find(CategoryEntity.class, categoryCode);
    }
}

