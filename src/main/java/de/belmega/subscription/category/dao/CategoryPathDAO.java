package de.belmega.subscription.category.dao;

import de.belmega.subscription.category.entities.CategoryPathEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class CategoryPathDAO {

    @PersistenceContext
    private EntityManager em;

    public void createNewPath(CategoryPathEntity path) {
        em.persist(path);
    }

    /**
     * Find all Paths that contain the given category code.
     */
    public List<CategoryPathEntity> findAllPathsForCategory(String code) {
        TypedQuery<CategoryPathEntity> query =
                em.createNamedQuery("path.findAllForCategory", CategoryPathEntity.class);

        query.setParameter("cat", code);

        return query.getResultList();
    }

    /**
     * Find the path that starts at the given category.
     */
    public CategoryPathEntity findPathByCategoryCode(String categoryCode) {
        return em.find(CategoryPathEntity.class, categoryCode);
    }
}
