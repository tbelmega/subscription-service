package de.belmega.subscription.category.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Since we are going to calculate the path whenever a category is created,
 * it needs to be stored in the database as an entity.
 */
@Entity
@Table(name = "categorypath")
@NamedQuery(name="path.findAllForCategory",
        query="SELECT DISTINCT p FROM CategoryPathEntity p JOIN FETCH p.codes c WHERE c = :cat")
public class CategoryPathEntity {

    /**
     * The unique identifier of a path is the code of the category where it is starting.
     */
    @Id
    private String rootCode;

    /**
     * A category path entity holds an (ordered) list of category codes.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> codes = new ArrayList<>();

    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }

    public String getRootCode() {
        return rootCode;
    }

    public void setRootCode(String code) {
        this.rootCode = code;
    }

    @Override
    public String toString() {
        return "CategoryPathEntity{" +
                "rootCode='" + rootCode + '\'' +
                ", codes=" + codes +
                '}';
    }
}
