package de.belmega.subscription.category.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * An instance of this class represents a Category from teh problem definition.
 * It is an Entity managed by JPA, so it maps directly to a database entry.
 */
@Entity
@Table(name = "category")
public class CategoryEntity {

    /**
     * The unique identifier of category objects is the code.
     */
    @Id
    private String code;

    private String title;

    private String superCategoryCode;

    /**
     * Constructor for creating a category conveniently, setting code and title.
     */
    public CategoryEntity(String code, String title) {
        this.code = code;
        this.title = title;
    }

    /**
     * Default constructor, required by JPA.
     */
    public CategoryEntity() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSuperCategoryCode() {
        return superCategoryCode;
    }

    public void setSuperCategoryCode(String superCategoryCode) {
        this.superCategoryCode = superCategoryCode;
    }

    @Override
    public String toString() {
        return "CategoryEntity{" +
                "code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", superCategoryCode=" + superCategoryCode +
                '}';
    }
}
