package de.belmega.subscription.util;


import de.belmega.subscription.books.dao.BookDAO;
import de.belmega.subscription.books.entities.BookEntity;
import de.belmega.subscription.category.dao.CategoryDAO;
import de.belmega.subscription.category.entities.CategoryEntity;
import de.belmega.subscription.category.service.CategoryPathService;
import de.belmega.subscription.subscribers.dao.SubscriberDAO;
import de.belmega.subscription.subscribers.entities.SubscriberEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;


@ApplicationScoped
public class TestDataGenerator {

    @Inject
    private BookDAO bookDAO;

    @Inject
    private CategoryDAO categoryDAO;

    @Inject
    private CategoryPathService pathService;

    @Inject
    private SubscriberDAO subscriberDAO;

    /**
     * This method is executed once when the Application has started.
     * We use it to inject test data into the database.
     */
    public void setupTestData(@Observes @Initialized(ApplicationScoped.class) Object init) {

        CategoryEntity engineering = new CategoryEntity();
        engineering.setCode("ENGINEERING");
        engineering.setTitle("Engineering");
        categoryDAO.createNewCategory(engineering);
        pathService.createNewPath(engineering);

        CategoryEntity se = new CategoryEntity();
        se.setCode("SWE");
        se.setTitle("Software Engineering");
        se.setSuperCategoryCode(engineering.getCode());
        categoryDAO.createNewCategory(se);
        pathService.createNewPath(se);

        CategoryEntity oop = new CategoryEntity();
        oop.setCode("OOP");
        oop.setTitle("Object-oriented programming");
        oop.setSuperCategoryCode(se.getCode());
        categoryDAO.createNewCategory(oop);
        pathService.createNewPath(oop);

        CategoryEntity agile = new CategoryEntity();
        agile.setCode("AGILE");
        agile.setTitle("Agile Software Development");
        agile.setSuperCategoryCode(se.getCode());
        categoryDAO.createNewCategory(agile);
        pathService.createNewPath(agile);

        BookEntity book = new BookEntity();
        book.setTitle("Clean Code");
        book.getCategoryCodes().add(oop.getCode());
        book.getCategoryCodes().add(agile.getCode());
        bookDAO.createNewBook(book);

        SubscriberEntity subscriber = new SubscriberEntity();
        subscriber.setEmail("c.norris@pardertec.de");
        subscriber.getCategoryCodes().add(oop.getCode());
        subscriber.getCategoryCodes().add(agile.getCode());
        subscriberDAO.createNewSubscriber(subscriber);
    }
}
