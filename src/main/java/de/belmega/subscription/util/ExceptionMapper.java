package de.belmega.subscription.util;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * The ExceptionMapper catches all Exceptions that are subclasses of the given generic type argument (in this case, all Exceptionns).
 * It sends a plain http response which is build in the below method.
 *
 * We do not want a sophisticated exception handling concept in this small application, so we do not implement different
 * ExceptionMappers for different exceptions here.
 * We simply map every exception to a response code of 500 and send the exception message back to the user.
 */
@Provider
public class ExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception exception) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(exception.getMessage())
                .type(MediaType.APPLICATION_JSON_TYPE).build();
    }
}
