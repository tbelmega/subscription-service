package de.belmega.subscription.books.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "book")
@NamedQueries({
        @NamedQuery(name = "book.findByCategory",
                query = "SELECT DISTINCT b FROM BookEntity b JOIN FETCH b.categoryCodes c WHERE c = :cat"),
        @NamedQuery(name = "book.findByCategories",
                query = "SELECT DISTINCT b FROM BookEntity b JOIN FETCH b.categoryCodes c WHERE c IN (:cats)")
})
public class BookEntity {

    /**
     * Create a unique id for each book. Let JPA handle that.
     */
    @Id
    @GeneratedValue
    private long id;

    private String title;

    /**
     * Could use @ManyToMany relationship between Category and Book entities.
     * But that would not work with the direct JSON mapping, it would require to introduce transfer objects.
     * I'd like to keep it small and simple as long as possible: No transfer objects, which means no @ManyToMany
     */
    @ElementCollection
    private List<String> categoryCodes = new ArrayList<>();


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getCategoryCodes() {
        return categoryCodes;
    }

    public void setCategoryCodes(List<String> categoryCodes) {
        this.categoryCodes = categoryCodes;
    }

    @Override
    public String toString() {
        return "BookEntity{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", categoryCodes=" + categoryCodes +
                '}';
    }
}
