package de.belmega.subscription.books.dao;

import de.belmega.subscription.books.entities.BookEntity;
import de.belmega.subscription.category.entities.CategoryEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class BookDAO {

    @PersistenceContext
    private EntityManager em;

    public BookEntity createNewBook(BookEntity book) {
        checkPreconditions(book);

        em.persist(book);

        return book;
    }

    /**
     * BookEntity has an auto-generated ID, so there is no risk for ID collision.
     * But we want to check if the given categories do exist.
     */
    private void checkPreconditions(BookEntity book) {

        // for each category code in the book's categories, check if it exists.
        for (String categoryCode: book.getCategoryCodes()) {

            // em.find in a loop requires a lot of database queries, bad for performance.
            // but we will add a cache soon, then this queries are not actually going to the database.
            if (em.find(CategoryEntity.class, categoryCode) == null) {
                throw new IllegalArgumentException("No category with the given code in the database: " + categoryCode);
            }
        }
    }

    /**
     * @return all book entities that belong to the given category
     */
    public List<BookEntity> findBooksByCategory(String categoryCode) {

        TypedQuery<BookEntity> query =
                em.createNamedQuery("book.findByCategory", BookEntity.class);
        query.setParameter("cat", categoryCode);

        return query.getResultList();
    }

    /**
     * @return all book entities that belong to one of the given categories
     */
    public  List<BookEntity> findBooksByCategories(List<String> categories) {

        TypedQuery<BookEntity> query =
                em.createNamedQuery("book.findByCategories", BookEntity.class);
        query.setParameter("cats", categories);

        return query.getResultList();
    }
}
