package de.belmega.subscription.books.rest;


import de.belmega.subscription.books.dao.BookDAO;
import de.belmega.subscription.books.entities.BookEntity;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * For explaining documentation, see CategoryResource.java.
 */
@RequestScoped
@Path(BooksResource.PATH)
public class BooksResource {

    public static final String PATH = "/books";


    @Inject
    private BookDAO booksDAO;


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCategory(BookEntity book) throws Exception {

        book = booksDAO.createNewBook(book);

        URI location = determineLocationOfNewBook(book);
        return Response.created(location).entity(book).build();
    }


    private URI determineLocationOfNewBook(BookEntity book) throws URISyntaxException {
        return new URI(PATH + "/" + book.getId());
    }
}
