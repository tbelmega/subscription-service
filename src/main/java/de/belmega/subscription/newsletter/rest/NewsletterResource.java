package de.belmega.subscription.newsletter.rest;


import de.belmega.subscription.newsletter.service.NewsletterService;
import de.belmega.subscription.newsletter.transfer.NewsletterTO;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@RequestScoped
@Path(NewsletterResource.PATH)
public class NewsletterResource {

    public static final String PATH = "/newsletters";

    @Inject
    private NewsletterService newsletterService;


    /**
     * Get all newsletters that should be send to subscribers.
     * @return the list with all newsletter that can be puzzled together from the database
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllNewsletters() {

        List<NewsletterTO> newsletters = newsletterService.generateAllNewsletters();

        return Response.ok(newsletters).build();
    }

}
