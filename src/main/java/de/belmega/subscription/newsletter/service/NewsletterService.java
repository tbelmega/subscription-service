package de.belmega.subscription.newsletter.service;

import de.belmega.subscription.books.dao.BookDAO;
import de.belmega.subscription.books.entities.BookEntity;
import de.belmega.subscription.category.dao.CategoryPathDAO;
import de.belmega.subscription.category.entities.CategoryPathEntity;
import de.belmega.subscription.newsletter.transfer.NewsletterTO;
import de.belmega.subscription.newsletter.transfer.NotificationTO;
import de.belmega.subscription.subscribers.dao.SubscriberDAO;
import de.belmega.subscription.subscribers.entities.SubscriberEntity;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class NewsletterService {

    @Inject
    private SubscriberDAO subscriberDAO;

    @Inject
    private BookDAO bookDAO;

    @Inject
    private CategoryPathDAO pathDAO;

    /**
     * Generate all Newsletter records from the system's data:
     *
     * Find all the subscribers and call the createNewsletterForUser() method.
     */
    public List<NewsletterTO> generateAllNewsletters() {

        List<NewsletterTO> result = new ArrayList<>();

        List<SubscriberEntity> subscribers = subscriberDAO.findAllSubscribers();

        for (SubscriberEntity subscriber : subscribers)
            result.add(createNewsletterForUser(subscriber));

        return result;
    }

    /**
     * For the given user, create the newsletter:
     *
     * Find all the categories he is directly or indirectly interested in.
     * Find all books for these categories.
     * Create a notification for each book.
     * Assemble the newsletter object.
     */
    private NewsletterTO createNewsletterForUser(SubscriberEntity subscriber) {

        List<String> subcategoriesForUser = getSubcategoriesForSubscriber(subscriber);
        List<BookEntity> books = bookDAO.findBooksByCategories(subcategoriesForUser);

        List<NotificationTO> notifications = new ArrayList<>();
        for (BookEntity book: books)
            notifications.add(createNotification(subcategoriesForUser, book));

        return new NewsletterTO(notifications, subscriber.getEmail());
    }

    /**
     * Create a notification object for the given book.
     * Take into account which categories the user is interested in.
     * Do not list paths that are not interesting for the user.
     */
    private NotificationTO createNotification(List<String> subcategoriesForUser, BookEntity book) {
        NotificationTO notification = new NotificationTO(book.getTitle());

        for (String categoryCode: book.getCategoryCodes())
            if (subcategoriesForUser.contains(categoryCode))
                notification.addPath(pathDAO.findPathByCategoryCode(categoryCode).getCodes());

        return notification;
    }


    /**
     * Collect all categories the subscriber is interested in.
     */
    private List<String> getSubcategoriesForSubscriber(SubscriberEntity subscriber) {
        List<String> allSubcategoriesForSubscriber = new ArrayList<>();

        List<String> codesOfDirectInterest = subscriber.getCategoryCodes();

        for (String code: codesOfDirectInterest) {
            // TODO: preprocess all categories to improve performance?
            List<CategoryPathEntity> pathEntities = pathDAO.findAllPathsForCategory(code);

            for (CategoryPathEntity path: pathEntities)
                allSubcategoriesForSubscriber.add(path.getRootCode());
        }

        return allSubcategoriesForSubscriber;
    }


}
