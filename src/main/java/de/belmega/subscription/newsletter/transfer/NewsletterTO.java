package de.belmega.subscription.newsletter.transfer;

import java.util.List;

/**
 * A Newsletter is not an own database entry, so it's not an Entity, more like a Transfer Object (TO).
 */
public class NewsletterTO {

    private String recipient;
    private List<NotificationTO> notifications;

    public NewsletterTO(List<NotificationTO> notifications, String email) {
        this.notifications = notifications;
        this.recipient = email;
    }


    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public List<NotificationTO> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationTO> notifications) {
        this.notifications = notifications;
    }

    public void addNotification(NotificationTO notification) {
        this.notifications.add(notification);
    }
}
