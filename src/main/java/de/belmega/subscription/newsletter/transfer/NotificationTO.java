package de.belmega.subscription.newsletter.transfer;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class NotificationTO {

    @JsonProperty("book") // requirement: called "book" in JSON, but I want bookTitle internally since it's more precise
    private String bookTitle;


    @JsonProperty
    private List<List<String>> categoryPaths = new ArrayList<>();

    public NotificationTO(String title) {
        this.bookTitle = title;
    }


    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public void addPath(List<String> path) {
        categoryPaths.add(path);
    }

}
