package de.belmega.subscription.subscribers.dao;

import de.belmega.subscription.books.entities.BookEntity;
import de.belmega.subscription.category.entities.CategoryEntity;
import de.belmega.subscription.subscribers.entities.SubscriberEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class SubscriberDAO {

    @PersistenceContext
    private EntityManager em;

    public SubscriberEntity createNewSubscriber(SubscriberEntity subscriber) {
        checkPreconditions(subscriber);

        em.persist(subscriber);

        return subscriber;
    }

    /**
     * SubscriberEntity has an auto-generated ID, so there is no risk for ID collision.
     * But we want to check if the given categories do exist.
     */
    private void checkPreconditions(SubscriberEntity subscriberEntity) {

        // for each category code in the subscriberEntity's categories, check if it exists.
        for (String categoryCode: subscriberEntity.getCategoryCodes()) {

            // em.find in a loop requires a lot of database queries, bad for performance.
            // but we will add a cache soon, then this queries are not actually going to the database.
            if (em.find(CategoryEntity.class, categoryCode) == null) {
                throw new IllegalArgumentException("No category with the given code in the database: " + categoryCode);
            }
        }
    }

    /**
     * Find all subscriber entities from the database.
     */
    public List<SubscriberEntity> findAllSubscribers() {

        // Using a Named Query is less 'safe' then Criteria API, but way shorter and more readable.
        // IntelliJ Ultimate has even syntax highlighting and parsing for the query string, so it's not so error prone as in eclipse.
        TypedQuery<SubscriberEntity> query =
                em.createNamedQuery("subscriber.findAll", SubscriberEntity.class);

        return query.getResultList();
    }
}
