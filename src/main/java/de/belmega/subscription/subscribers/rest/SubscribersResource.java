package de.belmega.subscription.subscribers.rest;


import de.belmega.subscription.subscribers.entities.SubscriberEntity;
import de.belmega.subscription.subscribers.dao.SubscriberDAO;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * For explaining documentation, see CategoryResource.java.
 */
@RequestScoped
@Path(SubscribersResource.PATH)
public class SubscribersResource {

    public static final String PATH = "/subscribers";

    @Inject
    private SubscriberDAO subscribersDAO;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createSubscriber(SubscriberEntity subscriber) throws Exception {

        subscriber = subscribersDAO.createNewSubscriber(subscriber);

        URI location = determineLocationOfNewSubscriber(subscriber);
        return Response.created(location).entity(subscriber).build();
    }


    private URI determineLocationOfNewSubscriber(SubscriberEntity subscriber) throws URISyntaxException {
        return new URI(PATH + "/" + subscriber.getId());
    }
}
