package de.belmega.subscription.subscribers.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "subscriber")
@NamedQuery(name="subscriber.findAll",
        query="SELECT DISTINCT s FROM SubscriberEntity s JOIN FETCH s.categoryCodes") // join fetch = eagerly load categoryCodes
public class SubscriberEntity {

    /**
     * Create a unique id for each book. Let JPA handle that.
     */
    @Id
    @GeneratedValue
    private long id;

    private String email;

    @ElementCollection
    private List<String> categoryCodes = new ArrayList<>();


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String title) {
        this.email = title;
    }

    public List<String> getCategoryCodes() {
        return categoryCodes;
    }

    public void setCategoryCodes(List<String> categoryCodes) {
        this.categoryCodes = categoryCodes;
    }
}
