# Subscription service #

This README would normally document whatever steps are necessary to get your application up and running.

### How to start the application ###

* Requirements: Java 8 and Maven 3.3.5 or later
* Clone the repository 
* Use the maven command `mvn clean package wildfly-swarm:run`
* Alternatively: Download test release from the downloads section of this repo and start with `java -jar subscriptions-service-swarm.jar`

### How it works ###

* The application will start up an embedded Wildfly JavaEE server, listening for http requests on port 8080.
* It uses a H2 in-memory database, so when stopping the application the database is dropped.
* On startup, the application inserts some test data into the database for convenience.
